package com.verify.bovquier.stackoverflowsearcher.common

/**
 * @author bovquier
 * on 16.10.2017.
 */

interface ConnectionStateProvider {
    val isOnline: Boolean
}
