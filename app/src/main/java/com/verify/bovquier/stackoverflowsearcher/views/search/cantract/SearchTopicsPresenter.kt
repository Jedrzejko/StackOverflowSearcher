package com.verify.bovquier.stackoverflowsearcher.views.search.cantract

import com.orhanobut.logger.Logger
import com.verify.bovquier.stackoverflowsearcher.base.BasePresenter
import com.verify.bovquier.stackoverflowsearcher.model.SoTopics
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author bovquier
 * on 19.10.2017.
 */
class SearchTopicsPresenter(private val model: SearchTopicsModel,
                            private val contentRepository: ContentRepository) :
        BasePresenter<SearchTopicsView, SoTopics, SearchTopicsModel>() {
    private var searchPhrase: String = ""

    private var startPage: Int = 1

    private var hasMoreData: Boolean = false

    fun loadTopics(searchPhrase: String) {
        if (this.searchPhrase.toLowerCase() != searchPhrase.toLowerCase()) {
            view?.showProgress(true)
            startPage = 1
            view?.clearItems()
            this.searchPhrase = searchPhrase
            load(searchPhrase)
        }
    }

    private fun load(searchPhrase: String) {
        if (searchPhrase.isNotBlank()) {
            disposables.add(model.getTopics(this.searchPhrase)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { onTopicsDownloaded(it) },
                            { onDownloadFailed(it) },
                            { view?.showProgress(false) }
                    )
            )
        } else {
            onDownloadCanceled()
        }
    }

    override fun onDownloadFailed(throwable: Throwable?) {
        super.onDownloadFailed(throwable)
        searchPhrase = ""
    }

    private fun onDownloadCanceled() {
        view?.clearItems()
    }

    private fun onTopicsDownloaded(result: SoTopics) {
        hasMoreData = result.has_more ?: false
        view?.setData(result)
        updateTopics(result)
    }

    private fun updateTopics(result: SoTopics) {
        val topics = contentRepository.getTopics(searchPhrase)
        if (topics != null) {
            result.items?.let {
                (it as ArrayList).addAll(0, topics.items ?: listOf())
            }
        }
        contentRepository.topics = result
    }

    fun loadMore() {
        if (hasMoreData && !searchPhrase.isBlank()) {
            Logger.t("Adapter").i("Loading more: ${++startPage}")
            disposables.add(model.getTopics(searchPhrase = this.searchPhrase, startPage = startPage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { onTopicsDownloaded(it) },
                            { onDownloadFailed(it) },
                            { view?.showProgress(false) }
                    )
            )
        }
    }

    fun reload() {
        startPage = 1
        view?.showProgress(true)
        view?.clearItems()
        load(searchPhrase)
    }

    fun restoreData() {
        view?.clearItems()
        contentRepository.restoreData()
        searchPhrase = contentRepository.searchPhrase
        val topics = contentRepository.topics
        startPage = contentRepository.startPage
        topics?.let {
            onTopicsDownloaded(it)
        }
    }

    fun saveData() {
        contentRepository.startPage = startPage
        contentRepository.searchPhrase = searchPhrase
        contentRepository.saveData()
    }
}
