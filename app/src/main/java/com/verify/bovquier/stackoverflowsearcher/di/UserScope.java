package com.verify.bovquier.stackoverflowsearcher.di;

import javax.inject.Scope;

/**
 * @author bovquier
 *         on 19.10.2017.
 */

@Scope
public @interface UserScope {
}
