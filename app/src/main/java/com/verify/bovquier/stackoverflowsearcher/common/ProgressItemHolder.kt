package com.verify.bovquier.stackoverflowsearcher.common

import android.view.View
import com.verify.bovquier.stackoverflowsearcher.common.adapter.ListItemHolder
import com.verify.bovquier.stackoverflowsearcher.model.Item

/**
 * @author bovquier
 * on 22.10.2017.
 */
class ProgressItemHolder(itemView: View) : ListItemHolder<Item>(itemView) {
    override fun bindValue(item: Item) {
        //no-op
    }
}