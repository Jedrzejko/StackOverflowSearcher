package com.verify.bovquier.stackoverflowsearcher.api

import com.verify.bovquier.stackoverflowsearcher.model.SoTopics
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author bovquier
 * on 19.10.2017.
 */
interface SearchService {
    companion object {
        val PAGE_COUNT: Int = 25
    }
    @GET("/search")
    fun searchForTopics(@Query("page") startPage: Int = 1, @Query("pagesize") pageSize: Int = PAGE_COUNT,
                        @Query("order") query: String = "desc",
                        @Query("sort") sort: String = "activity",
                        @Query("tagged") tagged: String = "android;mvp;rx;retrofit",
                        @Query("nottagged") notTagged: String = "c#;scala;cobol",
                        @Query("intitle") inTitle: String,
                        @Query("site") site: String = "stackoverflow"): Observable<SoTopics>
}