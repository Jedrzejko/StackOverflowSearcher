package com.verify.bovquier.stackoverflowsearcher.base

import com.verify.bovquier.stackoverflowsearcher.api.ResponseError

/**
 * @author bovquier
 * on 15.09.2017.
 */

interface BaseContract {
    interface Model<out DM : BaseDataModel>

    interface View {
        fun showProgress(visible: Boolean)
        fun showErrorMsg(t: Throwable)
        fun showErrorMsg(t: String)
        fun showErrorDialog(error: ResponseError?)
    }

    interface Presenter<V : View, out DM : BaseDataModel, M : Model<DM>> {
        var view: V?
        fun stopService()
    }
}
