package com.verify.bovquier.stackoverflowsearcher.common

/**
 * @author bovquier
 * on 10.10.2017.
 */

class NotOkResponseException(msg: String) : Throwable(msg)
