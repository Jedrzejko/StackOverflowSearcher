package com.verify.bovquier.stackoverflowsearcher.di.search

import com.verify.bovquier.stackoverflowsearcher.app.ApplicationComponent
import com.verify.bovquier.stackoverflowsearcher.di.NetworkModule
import com.verify.bovquier.stackoverflowsearcher.di.UserScope
import com.verify.bovquier.stackoverflowsearcher.views.search.SearchTopicsFragment
import dagger.Component

/**
 * @author bovquier
 * on 19.10.2017.
 */

@UserScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf
(SearchTopicsModule::class, NetworkModule::class))
interface SearchTopicsComponent {
    fun inject(fragment: SearchTopicsFragment)
}
