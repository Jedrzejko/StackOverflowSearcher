package com.verify.bovquier.stackoverflowsearcher.model

import com.verify.bovquier.stackoverflowsearcher.base.BaseDataModel

data class SoTopics(var items: List<Item>?, var has_more: Boolean?, var quota_max: Int?,
                    var quota_remaining: Int?) : BaseDataModel