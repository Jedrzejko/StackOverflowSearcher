package com.verify.bovquier.stackoverflowsearcher.api

/**
 * @author bovquier
 * on 24.10.2017.
 */
data class ResponseError(val error_id: Int, val error_message: String, val error_name: String)