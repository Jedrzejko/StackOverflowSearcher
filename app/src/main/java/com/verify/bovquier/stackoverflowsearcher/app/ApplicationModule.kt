package com.verify.bovquier.stackoverflowsearcher.app

import android.content.Context

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author bovquier
 * on 16.09.2017.
 */

@Module
class ApplicationModule(private val mApplication: StackOverflowSearcherApp) {

    @Provides
    @Singleton
    fun provideApplication(): StackOverflowSearcherApp = mApplication

    @Provides
    @Singleton
    fun provideContext(): Context = mApplication
}
