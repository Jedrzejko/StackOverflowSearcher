package com.verify.bovquier.stackoverflowsearcher.app

import android.content.Context
import com.verify.bovquier.stackoverflowsearcher.di.UserScope

import javax.inject.Singleton

import dagger.Component

/**
 * @author bovquier
 * on 16.09.2017.
 */

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    val context: Context
    fun inject(application: StackOverflowSearcherApp)
}
