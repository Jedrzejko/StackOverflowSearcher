package com.verify.bovquier.stackoverflowsearcher.views.search.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.verify.bovquier.stackoverflowsearcher.R
import com.verify.bovquier.stackoverflowsearcher.common.ProgressItemHolder
import com.verify.bovquier.stackoverflowsearcher.common.adapter.ListItemHolder
import com.verify.bovquier.stackoverflowsearcher.model.Item
import com.verify.bovquier.stackoverflowsearcher.model.SoTopics
import com.verify.bovquier.stackoverflowsearcher.model.TopicItemType

/**
 * @author bovquier
 * on 22.10.2017.
 */
class TopicsAdapter : RecyclerView.Adapter<ListItemHolder<Item>>() {
    private val progressItem: Item = Item(itemType = TopicItemType.PROGRESS_ITEM)
    private var topics: ArrayList<Item> = ArrayList()

    private lateinit var listener: OpenTabClickListener

    override fun getItemCount(): Int = topics.size

    override fun onBindViewHolder(holder: ListItemHolder<Item>, position: Int) {
        holder.bindValue(topics[position])
    }

    override fun getItemViewType(position: Int): Int {
        return topics[position].itemType.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemHolder<Item> {
        val inflater = LayoutInflater.from(parent.context)
        val itemType = TopicItemType.values()[viewType]
        return when (itemType) {
            TopicItemType.PROGRESS_ITEM -> {
                val itemView = inflater.inflate(R.layout.progress_item, parent, false)
                ProgressItemHolder(itemView)
            }
            TopicItemType.LIST_ITEM -> {
                val itemView = inflater.inflate(R.layout.item_topic, parent, false)
                TopicViewHolder(itemView, listener)
            }
        }
    }

    fun updateData(result: SoTopics) {
        result.items?.let {
            val lastIndex = topics.lastIndex
            if (topics.isEmpty()) {
                setItems(it)
            } else {
                addItems(lastIndex, it)
            }
            if (result.has_more == false) {
                removeProgressItem()
            }
        }
    }

    private fun removeProgressItem() {
        val index = topics.lastIndex
        if (topics[index].itemType == TopicItemType.PROGRESS_ITEM) {
            topics.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    private fun addItems(lastIndex: Int, it: List<Item>) {
        topics.addAll(lastIndex, it)
        notifyItemRangeInserted(lastIndex, it.size)
    }

    private fun setItems(it: List<Item>) {
        topics.addAll(it)
        topics.add(progressItem)
        notifyDataSetChanged()
    }

    fun clearItems() {
        topics.clear()
        notifyDataSetChanged()
    }

    fun addOpenTabClickListener(listener: OpenTabClickListener) {
        this.listener = listener
    }
}

interface OpenTabClickListener {
    fun onTopicClicked(topicUrl: String?)
}
