package com.verify.bovquier.stackoverflowsearcher.model

import com.verify.bovquier.stackoverflowsearcher.base.BaseDataModel

data class Item(val tags: List<String>? = null, val owner: Owner? = null,
                val is_answered: Boolean? = null, val view_count: Int? = null,
                val score: Int? = null, val last_activity_date: Int? = null,
                val creation_date: Int? = null, val last_edit_date: Int? = null,
                val question_id: Int? = null, val link: String? = null, val title: String? = null,
                val answer_count: Int? = null,
                val itemType: TopicItemType = TopicItemType.LIST_ITEM) : BaseDataModel

enum class TopicItemType {
    LIST_ITEM, PROGRESS_ITEM
}