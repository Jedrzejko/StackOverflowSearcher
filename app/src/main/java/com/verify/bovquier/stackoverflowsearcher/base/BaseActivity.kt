package com.verify.bovquier.stackoverflowsearcher.base

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager

/**
 * @author bovquier
 * on 15.09.2017.
 */

abstract class BaseActivity : AppCompatActivity(), BaseActivityView {

    override fun hideKeyboard() {
        val inputManager = getSystemService(
                Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (currentFocus != null) {
            inputManager.hideSoftInputFromWindow(currentFocus.windowToken,
                    InputMethodManager.RESULT_UNCHANGED_SHOWN
            )
        }
    }

    override fun setViewTitle(name: String) {
        title = name
    }
}
