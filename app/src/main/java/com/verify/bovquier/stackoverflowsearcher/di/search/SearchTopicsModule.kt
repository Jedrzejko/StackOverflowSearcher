package com.verify.bovquier.stackoverflowsearcher.di.search

import com.verify.bovquier.stackoverflowsearcher.api.SearchService
import com.verify.bovquier.stackoverflowsearcher.views.search.adapter.TopicsAdapter
import com.verify.bovquier.stackoverflowsearcher.views.search.cantract.ContentRepository
import com.verify.bovquier.stackoverflowsearcher.views.search.cantract.SearchTopicsModel
import com.verify.bovquier.stackoverflowsearcher.views.search.cantract.SearchTopicsPresenter
import dagger.Module
import dagger.Provides

/**
 * @author bovquier
 * on 19.10.2017.
 */

@Module
internal class SearchTopicsModule {
    @Provides
    fun providePresenter(model: SearchTopicsModel, contentRepository: ContentRepository):
            SearchTopicsPresenter = SearchTopicsPresenter(model, contentRepository)

    @Provides
    fun provideContentRepo(): ContentRepository = ContentRepository()

    @Provides
    fun provideModel(service: SearchService): SearchTopicsModel = SearchTopicsModel(service)

    @Provides
    fun provideAdapter(): TopicsAdapter = TopicsAdapter()
}
