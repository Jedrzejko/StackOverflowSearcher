package com.verify.bovquier.stackoverflowsearcher.base

/**
 * @author bovquier
 * on 10.10.2017.
 */

interface BaseActivityView {
    fun hideKeyboard()
    fun setViewTitle(name: String)
}
