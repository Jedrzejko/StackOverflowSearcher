package com.verify.bovquier.stackoverflowsearcher.views.search

import android.net.Uri


import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.verify.bovquier.stackoverflowsearcher.R
import com.verify.bovquier.stackoverflowsearcher.base.BaseFragment
import com.verify.bovquier.stackoverflowsearcher.common.adapter.InfiniteScrollListener
import com.verify.bovquier.stackoverflowsearcher.model.SoTopics
import com.verify.bovquier.stackoverflowsearcher.views.search.adapter.OpenTabClickListener
import com.verify.bovquier.stackoverflowsearcher.views.search.adapter.TopicsAdapter
import com.verify.bovquier.stackoverflowsearcher.views.search.cantract.SearchTopicsPresenter
import com.verify.bovquier.stackoverflowsearcher.views.search.cantract.SearchTopicsView
import kotlinx.android.synthetic.main.fragment_search_topics.*
import javax.inject.Inject
/**
 * A placeholder fragment containing a simple view.
 */
class SearchTopicsFragment : BaseFragment(), SearchTopicsView, OpenTabClickListener {
    override fun showErrorMsg(t: Throwable) {
        Snackbar.make(fabRefresh, t.message.toString(), Snackbar.LENGTH_LONG).show()
    }

    @Inject
    lateinit var presenter: SearchTopicsPresenter

    @Inject
    lateinit var topicsAdapter: TopicsAdapter

    private lateinit var scrollListener: RecyclerView.OnScrollListener

    override fun attachViewToPresenter() {
        presenter.view = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        Logger.addLogAdapter(AndroidLogAdapter())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_topics, container, false)
    }

    override fun performInjection() {
        getApplication().searchTopicsComponent.inject(this)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topicsAdapter.addOpenTabClickListener(this)
        rvTopics.apply {
            val linearManager = LinearLayoutManager(context)
            layoutManager = linearManager
            scrollListener = InfiniteScrollListener({ presenter.loadMore() }, linearManager)
            addOnScrollListener(scrollListener)
            setHasFixedSize(true)
            adapter = topicsAdapter
        }
        srlSwipeRefresher.setOnRefreshListener { presenter.reload() }
        fabRefresh.setOnClickListener({ presenter.reload() })
        showWelcomeSnack()
    }

    private fun showWelcomeSnack() {
        val snack = Snackbar.make(fabRefresh, "Open search bar, enter query, press done/search",
                Snackbar.LENGTH_INDEFINITE)
        snack.setAction("Hide", { snack.dismiss() }).show()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_search_topics, menu)
        val svSearch = menu.findItem(R.id.search).actionView as SearchView
        svSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                svSearch.clearFocus()
                loadQuestions(s)
                return false
            }

            override fun onQueryTextChange(s: String) = true
        })
        super.onCreateOptionsMenu(menu, menuInflater)
    }

    override fun showProgress(visible: Boolean) {
        srlSwipeRefresher.isRefreshing = visible
    }

    private fun loadQuestions(searchPhrase: String) {
        presenter.loadTopics(searchPhrase)
    }

    override fun stopService() {
        presenter.stopService()
    }

    override fun setData(result: SoTopics) {
        topicsAdapter.updateData(result)
    }

    override fun clearItems() {
        topicsAdapter.clearItems()
    }

    override fun onTopicClicked(topicUrl: String?) {
        topicUrl?.let {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(context, Uri.parse(topicUrl))
        }
    }

    override fun restoreData() {
        presenter.restoreData()
    }

    override fun saveData() {
        presenter.saveData()
    }
}
