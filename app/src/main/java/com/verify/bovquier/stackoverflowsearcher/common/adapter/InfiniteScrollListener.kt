package com.verify.bovquier.stackoverflowsearcher.common.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

/**
 * @author bovquier
 * on 22.10.2017.
 */

class InfiniteScrollListener(private val func: () -> Unit,
                             private val layoutManager: LinearLayoutManager)
    : RecyclerView.OnScrollListener() {
    private var visibleItemCount = 0
    private var totalItemCount = 0

    private var pastVisibleItems: Int = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy > 0) {
            visibleItemCount = layoutManager.childCount
            totalItemCount = layoutManager.itemCount
            pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                func()
            }
        }
    }

}