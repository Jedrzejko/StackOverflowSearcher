package com.verify.bovquier.stackoverflowsearcher.model

import com.verify.bovquier.stackoverflowsearcher.base.BaseDataModel

data class Owner(var reputation: Int?, var userId: Int?, var user_type: String?,
                 var accept_rate: Int?, var profile_image: String?, var display_name: String?,
                 var link: String?) : BaseDataModel