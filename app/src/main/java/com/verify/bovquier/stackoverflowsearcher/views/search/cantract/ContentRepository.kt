package com.verify.bovquier.stackoverflowsearcher.views.search.cantract

import com.orhanobut.hawk.Hawk
import com.verify.bovquier.stackoverflowsearcher.model.SoTopics

/**
 * @author bovquier
 * on 23.10.2017.
 */
class ContentRepository {
    companion object {
        val KEY_SEARCH_PHRASE: String = "KEY_SEARCH_PHRASE"
        val KEY_LAST_PAGE_SEARCHED: String = "KEY_LAST_PAGE_SEARCHED"
    }

    var topics: SoTopics? = null
    var searchPhrase: String = ""
    var startPage: Int = 1

    fun getTopics(searchPhrase: String?): SoTopics? = Hawk.get(searchPhrase)

    private fun saveTopics(searchPhrase: String, body: SoTopics) = Hawk.put(searchPhrase, body)

    private fun getLastSearchedPhrase(): String = Hawk.get(KEY_SEARCH_PHRASE, "")

    private fun saveLastSearchedPhrase(searchPhrase: String) = Hawk.put(KEY_SEARCH_PHRASE, searchPhrase)

    private fun saveLastPageSearched(startPage: Int) = Hawk.put(KEY_LAST_PAGE_SEARCHED, startPage)

    private fun getLastSearchedPage(): Int = Hawk.get(KEY_LAST_PAGE_SEARCHED, 1)

    private fun clear() = Hawk.deleteAll()

    fun restoreData() {
        searchPhrase = getLastSearchedPhrase()
        startPage = getLastSearchedPage()
        topics = getTopics(searchPhrase)
        clear()
    }

    fun saveData() {
        if (topics != null && searchPhrase.isNotBlank()) {
            saveLastSearchedPhrase(searchPhrase)
            saveLastPageSearched(startPage)
            saveTopics(searchPhrase, topics!!)
        }
    }
}