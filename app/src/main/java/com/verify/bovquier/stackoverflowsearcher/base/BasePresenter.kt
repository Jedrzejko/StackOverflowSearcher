package com.verify.bovquier.stackoverflowsearcher.base

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.orhanobut.logger.Logger
import com.squareup.moshi.Moshi
import com.verify.bovquier.stackoverflowsearcher.api.ResponseError
import com.verify.bovquier.stackoverflowsearcher.base.BaseContract.View
import io.reactivex.disposables.CompositeDisposable

/**
 * @author bovquier
 * on 16.09.2017.
 */

abstract class BasePresenter<V : View, out DM : BaseDataModel, M : BaseContract.Model<DM>> :
        BaseContract.Presenter<V, DM, M> {
    protected val disposables = CompositeDisposable()
    override var view: V? = null

    override fun stopService() {
        disposables.clear()
    }

    protected open fun onDownloadFailed(throwable: Throwable?) {
        if (throwable is HttpException) {
            val string = throwable.response().errorBody()?.string()
            string?.let {
                Logger.i(string)
                val adapter = Moshi.Builder().build().adapter(ResponseError::class.java)
                val error: ResponseError? = adapter.fromJson(string)
                view?.showErrorDialog(error)
            }
        } else if (throwable != null) {
            view?.showErrorMsg(throwable)
        } else {
            view?.showErrorMsg("Something went wrong")
        }
    }

}
