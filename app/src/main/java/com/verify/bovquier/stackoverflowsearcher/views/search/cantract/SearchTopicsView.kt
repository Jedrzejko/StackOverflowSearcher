package com.verify.bovquier.stackoverflowsearcher.views.search.cantract

import com.verify.bovquier.stackoverflowsearcher.base.BaseContract
import com.verify.bovquier.stackoverflowsearcher.model.SoTopics

/**
 * @author bovquier
 * on 19.10.2017.
 */
interface SearchTopicsView : BaseContract.View {
    fun setData(result: SoTopics)
    fun clearItems()
}