package com.verify.bovquier.stackoverflowsearcher.views.search

import android.os.Bundle
import android.support.v4.app.Fragment
import com.verify.bovquier.stackoverflowsearcher.R
import com.verify.bovquier.stackoverflowsearcher.base.BaseActivity
import kotlinx.android.synthetic.main.activity_search_topics.*

class SearchTopicsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_topics)
        setSupportActionBar(toolbar)
        if (savedInstanceState == null) {
            replaceFragment(SearchTopicsFragment(), "MainSearchFragment")
        }
    }

    private fun replaceFragment(fragment: Fragment, name: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, name).addToBackStack(name).commit()
    }
}
