package com.verify.bovquier.stackoverflowsearcher.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * @author bovquier
 * on 16.10.2017.
 */

class ConnectionStateProvideImpl(private val context: Context) : ConnectionStateProvider {
    override val isOnline: Boolean
        get() {
            val cm = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo: NetworkInfo? = cm.activeNetworkInfo
            return netInfo?.isConnectedOrConnecting ?: false
        }
}
