package com.verify.bovquier.stackoverflowsearcher.app

import android.app.Application
import com.orhanobut.hawk.Hawk
import com.verify.bovquier.stackoverflowsearcher.di.search.DaggerSearchTopicsComponent
import com.verify.bovquier.stackoverflowsearcher.di.search.SearchTopicsComponent

/**
 * @author bovquier
 * on 16.09.2017.
 */

class StackOverflowSearcherApp : Application() {

    companion object {
        lateinit var component: ApplicationComponent
    }

    internal lateinit var searchTopicsComponent: SearchTopicsComponent

    override fun onCreate() {
        super.onCreate()
        Hawk.init(this).build()
        Hawk.deleteAll()
        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
        searchTopicsComponent = DaggerSearchTopicsComponent.builder().applicationComponent(component).build()
    }
}
