package com.verify.bovquier.stackoverflowsearcher.views.search.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.verify.bovquier.stackoverflowsearcher.common.adapter.ListItemHolder
import com.verify.bovquier.stackoverflowsearcher.model.Item
import kotlinx.android.synthetic.main.item_topic.view.*

/**
 * @author bovquier
 * on 22.10.2017.
 */
class TopicViewHolder(itemView: View, private val listener: OpenTabClickListener?) :
        ListItemHolder<Item>(itemView) {
    override fun bindValue(item: Item) {
        itemView.tvTopic.text = item.title
        itemView.tvAnswerCount.text = item.answer_count.toString()
        itemView.tvUserName.text = item.owner?.display_name
        Glide.with(itemView.context).load(item.owner?.profile_image).into(itemView.ivAvatar)
        itemView.setOnClickListener { openItem(item.link) }
    }

    private fun openItem(link: String?) {
        listener?.onTopicClicked(link)
    }
}