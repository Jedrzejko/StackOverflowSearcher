package com.verify.bovquier.stackoverflowsearcher.common.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.verify.bovquier.stackoverflowsearcher.base.BaseDataModel

/**
 * @author bovquier
 * on 22.10.2017.
 */
abstract class ListItemHolder<in V : BaseDataModel>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bindValue(item: V)
}