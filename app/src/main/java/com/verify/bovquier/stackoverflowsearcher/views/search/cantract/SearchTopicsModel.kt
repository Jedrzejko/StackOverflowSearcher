package com.verify.bovquier.stackoverflowsearcher.views.search.cantract

import com.verify.bovquier.stackoverflowsearcher.api.SearchService
import com.verify.bovquier.stackoverflowsearcher.base.BaseContract
import com.verify.bovquier.stackoverflowsearcher.model.SoTopics
import io.reactivex.Observable

/**
 * @author bovquier
 * on 19.10.2017.
 */
class SearchTopicsModel(private val topicsService: SearchService) : BaseContract.Model<SoTopics> {
    fun getTopics(searchPhrase: String, startPage: Int = 1): Observable<SoTopics> =
            topicsService.searchForTopics(inTitle = searchPhrase, startPage = startPage)
}