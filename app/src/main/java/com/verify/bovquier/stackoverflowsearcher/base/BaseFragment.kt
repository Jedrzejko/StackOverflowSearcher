package com.verify.bovquier.stackoverflowsearcher.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.verify.bovquier.stackoverflowsearcher.api.ResponseError
import com.verify.bovquier.stackoverflowsearcher.app.StackOverflowSearcherApp

/**
 * @author bovquier
 * on 10.10.2017.
 */

abstract class BaseFragment : Fragment(), BaseContract.View {

    companion object {
        val KEY_HAS_SAVED_DATA = "KEY_HAS_SAVED_DATA"
    }

    /*private val parentView: BaseActivityView
        get() = activity as BaseActivityView*/

    override fun onResume() {
        super.onResume()
        attachViewToPresenter()
        if (restoreState) restoreData()
    }

    private var restoreState: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performInjection()
        restoreState = savedInstanceState?.getBoolean(KEY_HAS_SAVED_DATA, false) ?: false
    }

    abstract fun performInjection()

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBoolean(KEY_HAS_SAVED_DATA, true)
        saveData()
    }

    abstract fun saveData()

    abstract fun restoreData()

    override fun onDestroyView() {
        super.onDestroyView()
        stopService()
    }

    abstract fun stopService()

    protected abstract fun attachViewToPresenter()

    protected fun getApplication() = activity.application as StackOverflowSearcherApp

    override fun showErrorDialog(error: ResponseError?) {
        val dialogBuilder = AlertDialog.Builder(context).create()
        error?.let {
            dialogBuilder.apply {
                setTitle("${error.error_id} : ${error.error_name}")
                setMessage(error.error_message)
                setButton(AlertDialog.BUTTON_POSITIVE, "OK", { di, i -> dismiss() })
            }.show()
        }
    }

    override fun showErrorMsg(t: String) {
        Toast.makeText(context, t, Toast.LENGTH_LONG).show()
    }
}
